// $Id: README.txt,v 1.12.2.4 2011/01/07 01:42:48 Ebizon Team Exp $

EasyApns Module
----------------------

To install, place the entire ebi_easyapns folder into your site/all/modules directory.
Go to Administer -> Site building -> Modules and enable the EasyApns Module. 


Now to configure the module, go to Administer -> Site configuration -> Easy Apple Push Notification.
Insert the values in the fields as device name, device model, application name, application version 
Under the field certificate please enter the complete file path where your certifcate is placed on the disk.

To use this module First you have to add the content profile module and cck module.
After enabling the above modules create a field under the 'Profile' content type with name as 'field_apple_id' with Label as 'Apple ID' and of type 'text'
and then click on "Edit" for content-type profile and check checkbox "Use this content type as a content profile for users". 

After that click on "Content Profile" and then check "Use on administrative user creation form" checkbox and submit it.


Now go to Administer -> User Management -> Add user 
create a new user here and under the field apple id please insert the device token of iPhone/iPad to which you want to send notification.

After that open baseurl/pushnotif/userid/hellotest/type:NH 
replace userid with newly created user uid and type your message in place of hellotest. 

Dependancy
----------------------
Content profile module 
CCK Module 

Maintainers
----------------------
Ebizon Team
